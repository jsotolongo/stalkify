'use strict';

angular.module('Stalkify', []);

angular.module('Stalkify').controller('TagsController', function ($http, $scope) {
    var username;
    $scope.tags = [];
    $scope.selected = 0;

    $scope.add = function (tag) {
        $scope.selected++;
        tag.selected = true;
    };

    $scope.remove = function (tag) {
        $scope.selected--;
        tag.selected = false;
    };

    $scope.reset = function () {
        $scope.tags = [];
        $scope.selected = 0;
        loadTags();
    };

    $scope.filter = function () {
        var selected = [];
        angular.forEach($scope.tags, function (tag) {
            if (tag.selected) {
                selected.push(tag.tag);
            }
        });

        var params = getParams();
        params['tags'] = selected.join(',');

        window.location.href = '/?' + $.param(params);
    };

    /**
     * turns query string to JSON
     */
    var getParams = function () {
        var pieces = window.location.href.split('?');
        if (pieces.length < 2) {
            return {};
        }

        var queryString = pieces[1];
        var pairs = queryString.split('&');
        var params = {};

        angular.forEach(pairs, function (pair) {
            var keyValue = pair.split('=');
            params[keyValue[0]] = keyValue[1];
        });

        return params;
    };

    /**
     * loads all the tags used by the given user
     * if username is not part of query string then backend will use default username
     */
    var loadTags = function () {
        var params = getParams();

        // get selected tags
        var tags = [];
        if (params.hasOwnProperty('tags')) {
            tags = decodeURIComponent(params['tags']).split(',');
        }

        var url = '/tags';
        // get username
        if (params.hasOwnProperty('username')) {
            username = params['username'];
            url += '?username=' + username;
        }
        $http.get(url).then(function (response) {
            $scope.tags = response.data;

            // if there were any selected tags
            // preselect them on load to be displayed correctly
            // if in my request the tag "foo", then "foo" should appear already selected in the front end
            if (tags.length > 0) {
                angular.forEach($scope.tags, function (tag) {
                    if (tags.indexOf(tag.tag) > -1) {
                        $scope.add(tag);
                    }
                })
            }
        });
    };

    loadTags();
});