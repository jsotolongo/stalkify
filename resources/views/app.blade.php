<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>NinjaCat Test</title>

    <!-- Bootstrap Core CSS -->
    <link href="/assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="/assets/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- DataTables CSS -->
    <link href="/assets/vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="/assets/vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="/assets/dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="/assets/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>

<div id="wrapper">

    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-brand" href="/">Stalkify</a>
        </div>
    </nav>

    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">{{'@'.$username}}</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        @if (count($tweets) > 0)
            <!-- /.row -->
            <button class="btn btn-link" href="#filter-tags" data-toggle="modal">Filter by tags</button>

            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                <tr>
                                    <th style="width: 150px;">Date</th>
                                    <th>Tweet</th>
                                    <th style="width: 100px;">
                                        <!-- switch to icon when screen size is too small -->
                                        <span class="hidden-xs hidden-sm">Likes</span>
                                        <span class="hidden-md hidden-lg"><i class="fa fa-heart"></i></span>
                                    </th>
                                    <th style="width: 100px;">
                                        <!-- switch to icon when screen size is too small -->
                                        <span class="hidden-xs hidden-sm">Retweets</span>
                                        <span class="hidden-md hidden-lg"><i class="fa fa-retweet"></i></span>
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($tweets as $i => $tweet)
                                    <tr class="{{$i % 2 ? 'even' : 'odd'}}">
                                        <td>
                                            <!--
                                            this forces the sort to take into the account the timestamp
                                            therefore sorting correctly
                                            otherwise April posts were showing up before March when sorting ascending
                                            because A comes before M
                                            -->
                                            <span class=hide>{{ $tweet->date->getTimestamp() }}</span>
                                            {{ $tweet->date->format('F jS, Y') }}
                                        </td>
                                        <td>{{$tweet->content}}</td>
                                        <td>{{$tweet->likes}}</td>
                                        <td>{{$tweet->retweets}}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        @else
            <div class="alert alert-info">
                <i class="fa fa-info-circle"></i> No tweets were loaded.
                <a href="{{route('load-tweets', ['username' => $username])}}">Collect tweets.</a>
            </div>
        @endif
    </div>
    <!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->

<!-- load angularjs -->
<script src="/assets/vendor/angularjs/angularjs.js"></script>

<!-- jQuery -->
<script src="/assets/vendor/jquery/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="/assets/vendor/bootstrap/js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="/assets/vendor/metisMenu/metisMenu.min.js"></script>

<!-- DataTables JavaScript -->
<script src="/assets/vendor/datatables/js/jquery.dataTables.min.js"></script>
<script src="/assets/vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
<script src="/assets/vendor/datatables-responsive/dataTables.responsive.js"></script>

<!-- Custom Theme JavaScript -->
<script src="/assets/dist/js/sb-admin-2.js"></script>

<!-- Custom Angular code -->
<script src="/assets/dist/js/app.js"></script>

<!-- Page-Level Demo Scripts - Tables - Use for reference -->
<script>
    $(document).ready(function () {
        $('#dataTables-example').DataTable({
            order: [[0, 'desc']],
            responsive: true
        });
    });
</script>

<div id="filter-tags" class="modal fade" tabindex="-1" ng-app="Stalkify" ng-controller="TagsController">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
                <h4 class="modal-title">Filter by tag</h4>
            </div>
            <div class="modal-body">
                <div ng-if="tags.length == 0" class="alert alert-info">
                    <i class="fa fa-info-circle"></i> There were no tags found in this user's tweets.
                </div>
                <div ng-if="tags.length > 0">
                    <div class="alert alert-info">
                        <i class="fa fa-info-circle"></i> Filter out your tweets by only seen the tweets with the selected tags.
                    </div>
                    <div class="tags-pool">
                        <div id="included" class="well well-sm">
                            <div class="text-muted" ng-if="selected == 0">
                                Click on any tag you want to include<br>
                                Only tweets with these tags will be displayed
                            </div>

                            <div class="label label-default" ng-repeat="tag in tags" ng-if="tag.selected"
                                    ng-click="remove(tag)">
                                <span ng-bind="tag.tag"></span>
                                <span class="badge" ng-bind="tag.countOfTweets"></span>
                            </div>
                        </div>
                        <div id="all">
                            <div class="label label-default" ng-repeat="tag in tags" ng-if="!tag.selected"
                                    ng-click="add(tag)">
                                <span ng-bind="tag.tag"></span>
                                <span class="badge" ng-bind="tag.countOfTweets"></span>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal" ng-click="reset()">Close</button>
                <button type="button" class="btn btn-primary" ng-click="filter()">Filter</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

</div>

</body>

</html>
