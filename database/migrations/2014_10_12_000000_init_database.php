<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InitDatabase extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tweets', function (Blueprint $table) {
            $table->increments('id');
            $table->string('tweet_id');
            $table->string('user');
            $table->string('content');
            $table->integer('likes');
            $table->integer('retweets');
            $table->dateTime('date');
        });

        Schema::create('tags', function (Blueprint $table) {
            $table->increments('id');
            $table->string('tag');
        });

        Schema::create('tweet_tags', function (Blueprint $table) {
            $table->unsignedInteger('tweet_id');
            $table->unsignedInteger('tag_id');
            $table->unique(['tweet_id', 'tag_id']);
            $table->foreign('tweet_id')->references('id')->on('tweets');
            $table->foreign('tag_id')->references('id')->on('tags');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tweet_tag');
        Schema::dropIfExists('tags');
        Schema::dropIfExists('tweets');
    }
}
