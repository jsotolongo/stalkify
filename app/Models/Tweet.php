<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Tweet
 * @package app\Models
 */
class Tweet extends Model
{
    /**
     * @var bool
     */
    public $timestamps = false;

    public $dates = [
        'date'
    ];

    /**
     * @var array
     */
    protected $fillable = [
        'tweet_id',
        'user',
        'content',
        'likes',
        'retweets',
        'date'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasManyThrough
     */
    public function tags()
    {
        return $this->hasManyThrough(Tag::class, TweetTag::class);
    }
}