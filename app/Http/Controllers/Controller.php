<?php

namespace App\Http\Controllers;

use Abraham\TwitterOAuth\TwitterOAuth;
use App\Models\Tag;
use App\Models\Tweet;
use App\Models\TweetTag;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

/**
 * Class Controller
 * @package App\Http\Controllers
 */
class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $username = request()->get('username', USERNAME);

        // get base query
        $query = Tweet::where('user', '=', $username)->orderBy('date', 'desc');

        // if tags are filtered out then add additional logic to the query to only
        // get what i need
        $tags = request()->get('tags');
        if (!empty($tags)) {
            $tags = explode(',', $tags);
            $query->join('tweet_tags', 'tweets.id', '=', 'tweet_tags.tweet_id')
                ->join('tags', 'tweet_tags.tag_id', '=', 'tags.id')
                ->whereIn('tags.tag', $tags);
        }

        $tweets = $query->get();

        return view('app', [
            'username'   => $username,
            'tweets' => $tweets,
        ]);
    }

    /**
     * @return array
     */
    public function tags()
    {
        $username = request()->get('username', USERNAME);

        // SELECT tags.*, COUNT(tags.id) as countOfTweets FROM tags
        // INNER JOIN tweet_tags ON tags.id=tweet_tags.tag_id
        // INNER JOIN tweets ON tweet_tags.tweet_id=tweets.id
        // WHERE tweets.user = 'marvel'
        // GROUP BY tags.id, tags.tag ORDER BY countOfTweets DESC
        $tags = \App\Models\Tag::select(['tags.*', \Illuminate\Support\Facades\DB::raw('COUNT(tags.id) as countOfTweets')])
            ->join('tweet_tags', 'tags.id', '=', 'tweet_tags.tag_id')
            ->join('tweets', 'tweet_tags.tweet_id', '=', 'tweets.id')
            ->where('tweets.user', '=', $username)
            ->groupBy(\Illuminate\Support\Facades\DB::raw('tags.id, tags.tag'))->orderBy('countOfTweets', 'DESC')->get();

        return $tags;
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function stalking()
    {
        $username = request()->get('username', USERNAME);

        $monthAgo = (new \DateTime())->modify('-1 month');

        $twitter = new TwitterOAuth(
            env('TWITTER_APP_ID'),
            env('TWITTER_APP_SECRET'),
            env('TWITTER_TOKEN'),
            env('TWITTER_TOKEN_SECRET')
        );

        $request = [
            'screen_name'     => $username,
            'count'           => 200,
            'trim_user'       => false,
            'exclude_replies' => true,
            'include_rts'     => false
        ];

        // keep loop going until we get to "one month" ago
        // if the loop hasn't ended after 10 tries, exit; very unlikely but we don't want to be stuck in the loop
        $count = 0;
        while ($count < 10) {
            $count++;

            try {
                $data = $twitter->get('statuses/user_timeline', $request);
            }
            catch (\Exception $e) {
                // if the request to twitter fails, make $data null which will make it redirect to list-tweets
                $data = null;
            }

            // no more tweets left
            if (empty($data)) {
                return redirect()->route('list-tweets', ['username' => $username]);
            }

            // save tweets
            foreach ($data as $datum) {
                $request['max_id'] = $datum->id_str;

                // if twitter is past this month; redirect to tweets views
                $date = new \DateTime($datum->created_at);
                if ($date->getTimestamp() < $monthAgo->getTimestamp()) {
                    return redirect()->route('list-tweets', ['username' => $username]);
                }

                // if tweet already exists, skip
                $tweet = Tweet::where('tweet_id', '=', $datum->id_str)->first();
                if ($tweet != null) {
                    continue;
                }

                // insert tweet
                $tweet = new Tweet();
                $tweet->fill([
                    'tweet_id' => $datum->id_str,
                    'user'     => $datum->user->screen_name,
                    'content'  => $datum->text,
                    'likes'    => $datum->favorite_count,
                    'retweets' => $datum->retweet_count,
                    'date'     => $date->format('Y-m-d H:i:s')
                ]);
                $tweet->save();

                // save hastags
                foreach ($datum->entities->hashtags as $hashtag) {
                    // try to find the tag
                    $tag = Tag::where('tag', '=', $hashtag->text)->first();

                    // if tag doesn't exists create it
                    if ($tag == null) {
                        $tag = new Tag();
                        $tag->fill([
                            'tag' => $hashtag->text
                        ]);
                        $tag->save();
                    }

                    // try to find tweet-tag pair
                    $tweetTag = TweetTag::where([
                        'tweet_id' => $tweet->id,
                        'tag_id'   => $tag->id
                    ])->first();

                    // if tweet-tag pair doesn't exists create it
                    if ($tweetTag == null) {
                        TweetTag::insert([
                            'tweet_id' => $tweet->id,
                            'tag_id'   => $tag->id
                        ]);
                    }
                }
            }
        }

        return redirect()->route('list-tweets', ['username' => $username]);
    }
}
