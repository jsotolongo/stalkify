<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

const USERNAME = 'marvel';

Route::get('/', 'Controller@index')->name('list-tweets');
Route::get('/tags', 'Controller@tags')->name('list-tags');
Route::get('/stalking', 'Controller@stalking')->name('load-tweets');